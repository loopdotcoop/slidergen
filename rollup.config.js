// Configuration, used by `rollup -c` during `npm run build`.

import { babel } from '@rollup/plugin-babel';
import { terser } from 'rollup-plugin-terser';

export default [

    // Bundle the `Slidergen` class. Tree-shaking should remove all tests.
    {
        input: 'src/slidergen-main.js',
        output: {
            file: 'public/lib/slidergen.es.js',
            format: 'es', // eg for `node public/node-test.js`
        }
    },
    {
        input: 'src/slidergen-main.js',
        output: {
            file: 'public/lib/slidergen.umd.es5.min.js',
            format: 'umd', // eg for `public/index.html` in legacy browsers
            name: 'slidergen.main', // `const Slidergen = slidergen.main`
        },
        // See https://babeljs.io/docs/en/babel-preset-env
        // and https://github.com/terser/terser#minify-options
        plugins: [
            babel({ babelHelpers: 'bundled' }),
            terser({ keep_classnames:true })
        ]
    },

    // Bundle the example usage. Examples only work in browsers, not NodeJS.
    {
        input: 'src/slidergen-example.js',
        output: {
            file: 'public/lib/slidergen-example.es.js',
            format: 'es',
        }
    },
    {
        input: 'src/slidergen-example.js',
        output: {
            file: 'public/lib/slidergen-example.umd.es5.js',
            format: 'umd', // eg for `public/index.html` in legacy browsers
            name: 'slidergen.example', // `slidergen.example(Slidergen)`
        },
        plugins: [ babel({ babelHelpers: 'bundled' }) ]
    },

    // Bundle the tests. Tree-shaking should remove the `Slidergen` class.
    {
        input: 'src/slidergen-test.js',
        output: [
            {
                file: 'public/lib/slidergen-test.es.js',
                format: 'es', // eg for `node public/node-test.js`
            },
            {
                file: 'public/lib/slidergen-test.umd.js',
                format: 'umd', // eg for `public/test.html` in legacy browsers
                name: 'slidergen.test' // `slidergen.test(expect, Slidergen)`
            },
        ]
    }

];
