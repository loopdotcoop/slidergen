// slidergen/src/slidergen-example.js

// The ‘main’ file for bundling the Slidergen usage example.

// Run the usage example.
export default function slidergenExample(Slidergen) {
    const $log = document.querySelector('#log');
    const $form0 = document.querySelector('#form-0');
    const $form1 = document.querySelector('#form-1');
    if (! $log || ! $form0 || ! $form1) throw Error('Missing an element');

    // Generate the top form.
    const schema0 = {
        _meta: { title:'Top Form' },
        outer: {
            _meta: { title:'Outer' },
            a_boolean: Slidergen.boolean(false),
            another_boolean: Slidergen.boolean(true),
        },
        foo: Slidergen.boolean(true),
    }
    const sg0 = new Slidergen($form0, 'schema0', schema0);
    $log.innerHTML = `new Slidergen($form0, 'schema0', schema0) =>\n\n`;
    $log.innerHTML += JSON.stringify(sg0.toObject(), null, 2);

    // Generate the second form.
    const schema1 = {
        _meta: { title:'Second Form' },
        outer: {
            _meta: { title:'Outer' },
            foo: Slidergen.boolean(false),
            inner: {
                _meta: { title:'Inner' },
                bar: Slidergen.boolean(false),
                baz: Slidergen.boolean(true),
            },
            zub: Slidergen.boolean(false),
        },
    }
    new Slidergen($form1, 'schema1', schema1);
}
