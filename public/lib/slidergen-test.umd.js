(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, (global.slidergen = global.slidergen || {}, global.slidergen.test = factory()));
})(this, (function () { 'use strict';

    // slidergen/src/slidergen.js


    /* ---------------------------------- Test ---------------------------------- */

    // Runs basic tests on Slidergen.
    function testSlidergenBasics(expect, Slidergen) {
        const $el = document.createElement('div');
        expect().section('Slidergen basics');

        // Is a class.
        expect(`typeof Slidergen`, typeof Slidergen).toBe('function');

        // Invalid contructor arguments.
        expect(`new Slidergen()`,
                new Slidergen()).toHave({
                    err:`myFunction(): '$container' is not an HTMLElement` });
        expect(`new Slidergen($el)`,
                new Slidergen($el)).toHave({
                    err:`new Slidergen(): 'identifier' is type 'undefined' not 'string'` });
        expect(`new Slidergen($el, '1abc')`,
                new Slidergen($el, '1abc')).toHave({
                    err:`new Slidergen(): 'identifier' \"1abc\" fails /^[_a-z][_0-9a-z]*$/` });
        expect(`new Slidergen($el, 'abc')`,
                new Slidergen($el, 'abc')).toHave({
                    err:`new Slidergen(): 'schema' is type 'undefined' not an object` });
        expect(`new Slidergen($el, 'abc', {_meta:{},a:{kind:'number'},b:{kind:'nope!'}})`,
                new Slidergen($el, 'abc', {_meta:{},a:{kind:'number'},b:{kind:'nope!'}})).toHave({
                    err:`new Slidergen(): 'schema.b.kind' not recognised` });

        // Contructor arguments ok.
        expect(`new Slidergen($el, 'abc', {_meta:{title:'Abc'}})`,
                new Slidergen($el, 'abc', {_meta:{title:'Abc'}})).toHave({
                    $container: $el,
                    identifier: 'abc',
                    schema: { _meta:{title:'Abc'} },
                });
    }

    // slidergen/src/slidergen-test.js

    // Run each test. You can comment-out some during development, to help focus on
    // individual tests. But make sure all tests are uncommented before committing.
    function slidergenTest(assert, Slidergen) {

        // Mock a DOM, for NodeJS.
        if (typeof global === 'object') {
            global.HTMLElement = class HTMLElement {
                addEventListener() { }
                appendChild() { }
                createElement() { return new HTMLElement() }
            };
            HTMLElement.prototype.classList = { add() {} };
            HTMLElement.prototype.style = {};
            global.document = new HTMLElement();
        }

        testSlidergenBasics(assert, Slidergen);
    }

    return slidergenTest;

}));
