// slidergen/src/slidergen-main.js

// The ‘main’ file for bundling the `Slidergen` class.
// Tree-shaking should remove all tests.

import Slidergen from './slidergen.js';
export default Slidergen;
