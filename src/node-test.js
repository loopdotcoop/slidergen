// slidergen/src/node-test.js

/* -------------------------------- Imports --------------------------------- */

import expect from '../lib/expect.js';
import Slidergen from './slidergen-main.js';
import slidergenTest from './slidergen-test.js';


/* --------------------------------- Tests ---------------------------------- */

slidergenTest(expect, Slidergen);
console.log(expect().done());
