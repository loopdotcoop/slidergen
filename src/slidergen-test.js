// slidergen/src/slidergen-test.js

// The ‘main’ file for bundling Slidergen unit tests.

import { testSlidergenBasics, testTypicalUsage } from './slidergen.js';

// Run each test. You can comment-out some during development, to help focus on
// individual tests. But make sure all tests are uncommented before committing.
export default function slidergenTest(expect, Slidergen) {

    // Mock a DOM, for NodeJS.
    if (typeof global === 'object') {
        global.HTMLElement = class HTMLElement {
            addEventListener() { }
            appendChild() { }
            createElement() { return new HTMLElement() }
        };
        HTMLElement.prototype.classList = { add() {} }
        HTMLElement.prototype.style = {}
        global.document = new HTMLElement();
    }

    testSlidergenBasics(expect, Slidergen);
    testTypicalUsage(expect, Slidergen);
}
