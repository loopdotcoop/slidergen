// slidergen/src/slidergen.js

// Assembles the `Slidergen` class.


/* --------------------------------- Import --------------------------------- */

import { schemaToSteps, render } from './helpers/class-helpers.js';
import Validarg from '../lib/validarg.es.js';


/* ---------------------------------- Class --------------------------------- */

// Transforms an object schema to an HTML form.
// Run the ‘Usage Example’ for typical usage. Essentially:
//     new Slidergen(
//         document.querySelector('#wrap'),
//         'my_form',
//         {
//             _meta: { title:'My Form' },
//             outer: {
//                 _meta: { title:'Outer' },
//                 an_inner_boolean: Slidergen.boolean(false),
//                 another_boolean: Slidergen.boolean(true),
//             },
//             outer_boolean: Slidergen.boolean(true),
//         }
//     );
export default class Slidergen {

    constructor($container, identifier, schema) {
        // Validate and store the instantiation arguments.
        const v = new Validarg('new Slidergen()', false);
        if (!($container instanceof HTMLElement))
            return this.err = `myFunction(): '$container' is not an HTMLElement`;
        if (! v.string(identifier, 'identifier', /^[_a-z][_0-9a-z]*$/))
            return this.err = v.err;
        if (! v.schema(schema, 'schema'))
            return this.err = v.err;
        this.$container = $container;
        this.identifier = identifier;
        this.schema = schema;

        const [height, steps] = schemaToSteps(schema, identifier);
        this.height = height;
        this.steps = steps;
        render($container, steps);
        $container.style.height = `${height*30}px`;
    }

    static boolean(initially) {
        return {
            initially,
            kind: 'boolean',
        }
    }

    toObject() {
        return {
            height: this.height,
            schema: this.schema,
            steps: this.steps,
        }
    }
}


/* ---------------------------------- Test ---------------------------------- */

// Runs basic tests on Slidergen.
export function testSlidergenBasics(expect, Slidergen) {
    const $el = document.createElement('div');
    expect().section('Slidergen basics');

    // Is a class.
    expect(`typeof Slidergen`, typeof Slidergen).toBe('function');

    // Invalid contructor arguments.
    expect(`new Slidergen()`,
            new Slidergen()).toHave({
                err:`myFunction(): '$container' is not an HTMLElement` });
    expect(`new Slidergen($el)`,
            new Slidergen($el)).toHave({
                err:`new Slidergen(): 'identifier' is type 'undefined' not 'string'` });
    expect(`new Slidergen($el, '1abc')`,
            new Slidergen($el, '1abc')).toHave({
                err:`new Slidergen(): 'identifier' \"1abc\" fails /^[_a-z][_0-9a-z]*$/` });
    expect(`new Slidergen($el, 'abc')`,
            new Slidergen($el, 'abc')).toHave({
                err:`new Slidergen(): 'schema' is type 'undefined' not an object` });
    expect(`new Slidergen($el, 'abc', {_meta:{},a:{kind:'number'},b:{kind:'nope!'}})`,
            new Slidergen($el, 'abc', {_meta:{},a:{kind:'number'},b:{kind:'nope!'}})).toHave({
                err:`new Slidergen(): 'schema.b.kind' not recognised` });

    // Contructor arguments ok.
    expect(`new Slidergen($el, 'abc', {_meta:{title:'Abc'}})`,
            new Slidergen($el, 'abc', {_meta:{title:'Abc'}})).toHave({
                $container: $el,
                identifier: 'abc',
                schema: { _meta:{title:'Abc'} },
            });
}

// Runs typical usage tests. @TODO
export function testTypicalUsage(expect, Slidergen) {
    // expect().section('Typical usage');
}
