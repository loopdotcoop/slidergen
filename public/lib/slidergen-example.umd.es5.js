(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory() :
    typeof define === 'function' && define.amd ? define(factory) :
    (global = typeof globalThis !== 'undefined' ? globalThis : global || self, (global.slidergen = global.slidergen || {}, global.slidergen.example = factory()));
})(this, (function () { 'use strict';

    // slidergen/src/slidergen-example.js
    // The ‘main’ file for bundling the Slidergen usage example.
    // Run the usage example.
    function slidergenExample(Slidergen) {
      var $log = document.querySelector('#log');
      var $form0 = document.querySelector('#form-0');
      var $form1 = document.querySelector('#form-1');
      if (!$log || !$form0 || !$form1) throw Error('Missing an element'); // Generate the top form.

      var schema0 = {
        _meta: {
          title: 'Top Form'
        },
        outer: {
          _meta: {
            title: 'Outer'
          },
          a_boolean: Slidergen["boolean"](false),
          another_boolean: Slidergen["boolean"](true)
        },
        foo: Slidergen["boolean"](true)
      };
      var sg0 = new Slidergen($form0, 'schema0', schema0);
      $log.innerHTML = "new Slidergen($form0, 'schema0', schema0) =>\n\n";
      $log.innerHTML += JSON.stringify(sg0.toObject(), null, 2); // Generate the second form.

      var schema1 = {
        _meta: {
          title: 'Second Form'
        },
        outer: {
          _meta: {
            title: 'Outer'
          },
          foo: Slidergen["boolean"](false),
          inner: {
            _meta: {
              title: 'Inner'
            },
            bar: Slidergen["boolean"](false),
            baz: Slidergen["boolean"](true)
          },
          zub: Slidergen["boolean"](false)
        }
      };
      new Slidergen($form1, 'schema1', schema1);
    }

    return slidergenExample;

}));
