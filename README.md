# slidergen

__Transforms an object schema to an HTML form.__

▶&nbsp; __Version:__ 0.0.1  
▶&nbsp; __Repo:__ <https://gitlab.com/loopdotcoop/slidergen/>  
▶&nbsp; __Homepage:__ <https://loopdotcoop.gitlab.io/slidergen/>  


Run the ‘Usage Example’ for typical usage. Essentially:

```js
new Slidergen(
    document.querySelector('#wrap'),
    {
        _meta: { title:'My Form' },
        outer: {
            _meta: { title:'Outer' },
            an_inner_boolean: Slidergen.boolean(false),
            another_boolean: Slidergen.boolean(true),
        },
        outer_boolean: Slidergen.boolean(true),
    },
    'my_form'
);
```


## Check Global Dependencies

1.  Check your __git__ version:  
    `git --version # should be 'git version 2.30.0'`
2.  Check your __node__ version:  
    `node --version # should be 'v16.13.0'`
3.  Check your __npm__ version:  
    `npm --version # should be '8.1.0'`
4.  Check your __rollup__ version:  
    `rollup --version # should be 'rollup v2.75.7'`
5.  Check your __static-server__ version:  
    `static-server --version # should be 'static-server@2.2.1'`


## Running Usage Examples

Run the ‘src/’ usage example:  
`static-server & open http://localhost:9080/ && fg # test src/ using a browser`  

Run the ‘public/’ usage example, after a build:  
`static-server public & open http://localhost:9080/ && fg # test public/ using a browser`  


## Dev, Test and Build

Run the ‘src/’ test suite, during development:  
`node src/node-test.js # test src/ using NodeJS`
`static-server & open http://localhost:9080/test.html && fg # test src/ using a browser`  

Build the minified and unminified bundles, using settings in rollup.config.js:  
`rollup -c`  
or  
`npm run build`

Run the ‘public/’ test suite, after a build:  
`npm test # test public/ using NodeJS`  
`static-server public & open http://localhost:9080/test.html && fg # test public/ using a browser`  
