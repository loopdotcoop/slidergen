// slidergen/src/helpers/constants.js


/* -------------------------------- Constants ------------------------------- */

export const CSS_PREFIX = 'sg-'; // should have trailing '-'
export const ID_PREFIX = 'sg'; // should NOT have trailing '-'
