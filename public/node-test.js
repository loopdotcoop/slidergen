// slidergen/public/node-test.js

/* -------------------------------- Imports --------------------------------- */

import expect from '../lib/expect.js';
import Slidergen from './lib/slidergen.es.js';
import slidergenTest from './lib/slidergen-test.es.js';


/* --------------------------------- Tests ---------------------------------- */

slidergenTest(expect, Slidergen);
console.log(expect().done());
